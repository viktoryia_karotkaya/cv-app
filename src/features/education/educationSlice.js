import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";

const initialState = {
  education: [],
  status: 'idle',
  error: null
}

export const fetchEducation = createAsyncThunk(
  'education/fetchEducation',
  async () => {
    const response = await fetch('https://cv-app-c2851-default-rtdb.firebaseio.com/educations.json');
    if (response.ok){
      const data = await response.json();
      const dataArr = [];
      for (let key in data){
        dataArr.push({
          id: key,
          date: data[key].date,
          title: data[key].title,
          text: data[key].text
        })
      }
      return dataArr;
    } else {
      throw Error(response.statusText);
    }
    
  }
)

const education = createSlice({
  name: 'education',
  initialState: initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(fetchEducation.pending, (state, action) => {
        state.status = 'loading'
      })
      .addCase(fetchEducation.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.education = action.payload
      })
      .addCase(fetchEducation.rejected, (state, action) => {
        state.status = 'failed'
        state.error = "Something went wrong. Please review your server connection";
      })
  }
});

export const educationAction = education.actions;
export default education.reducer;
