import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";

const initialState = {
  skills: [],
  status: 'idle',
  error: null
}

export const fetchSkills = createAsyncThunk(
  'skills/fetchSkills',
  async () => {
    const response = await fetch('https://cv-app-c2851-default-rtdb.firebaseio.com/skills.json');
    if (response.ok){
      const data = await response.json();
      const dataArr = [];
      for (let key in data){
        dataArr.push({
          id: key,
          skillName: data[key].skillName,
          skillRange: data[key].skillRange
        })
      }
      return dataArr;
    } else {
      throw Error(response.statusText);
    }
  }
)

export const postSkills = createAsyncThunk(
  'skills/postSkills',
  async (skill) => {
    const response = await fetch('https://cv-app-c2851-default-rtdb.firebaseio.com/skills.json',
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(skill)
      });
    if (response.ok){
      return await response.json();
    } else {
      throw Error(response.statusText);
    }
  }
)

const skills = createSlice({
  name: 'skills',
  initialState: initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(fetchSkills.pending, (state, action) => {
        state.status = 'loading'
      })
      .addCase(fetchSkills.fulfilled, (state, action) => {
        state.status = 'succeeded'
        state.skills = action.payload
      })
      .addCase(fetchSkills.rejected, (state, action) => {
        state.status = 'failed'
        state.error = "Something went wrong. Please review your server connection";
      })
      .addCase(postSkills.fulfilled, (state, action) => {
        state.skills.push({...action.meta.arg,
        id: action.payload.name});
      })
      .addCase(postSkills.rejected, (state, action) => {
        state.status = 'failed'
        state.error = "Something went wrong. Please review your server connection";
      })
  }
});

export const skillsAction = skills.actions;
export default skills.reducer;
