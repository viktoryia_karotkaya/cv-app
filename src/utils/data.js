export const personalInfo = {
  name: "Viktoryia Karotkaya",
  title: "Front-End Engineer",
  description: "Passionate and hardworking Front-End Engineer with 2 years of experience developing commercial Apps using JavaScript, TypeScript, React, Redux, Angular, HTML5/CSS3, looking for an opportunity to continue building on my technical skill set."
};

export const navigation = [
  {item: 'About me', icon: 'faUser', id: 'About'},
  {item: 'Education', icon: 'faGraduationCap', id: 'Education'},
  {item: 'Experience', icon: 'faPen', id: 'Experience'},
  {item: 'Skills', icon: 'faGem', id: 'Skills'},
  {item: 'Portfolio', icon: 'faSuitcase', id: 'Portfolio'},
  {item: 'Contacts', icon: 'faLocationArrow', id: 'Contacts'},
  {item: 'Feedbacks', icon: 'faComment', id: 'Feedbacks'}
];

export const expertise = [
  {
    date: '2021-2022',
    info: {
      company: 'EPAM Systems',
      job: 'Software Engineer',
      description: 'Participated in the development of React based projects. Developed new UI functionality. Created cross browser markups from Figma design using Styled system and Emotion. Created a responsive layout. Participated in Scrum ceremonies such as grooming, planning, retrospective, and daily meetings. Unit test writing using Jest and React Testing Library. Technologies: JavaScript, React, Redux, Styled system, Emotion, Material UI, JSS, Figma, HTML, CSS.'
    }
  },
  {
    date: '2017',
    info: {
      company: 'SVAPS Systems',
      job: 'Junior Front-End Engineer',
      description: 'Developed and implemented native library for project goals. Developed new UI/UX functionality to create a more functional user experience.\n' +
        'Implemented authentication and authorization (user roles) in App.\n' +
        'Implemented new type of payment method to expand and streamline the payment process.\n' +
        'Integrated with RESTful APIs for server-side functionality.\n' +
        'Technologies: JavaScript, Angular 2, TypeScript, RxJS, HTML5 / CSS3.'
    }
  }
];

export const portfolio = [
  {src: "Code", title: "Good Morning", text: "Online coffee-shop store was built using React + Redux (Class - based components).", link: "https://goodmorning-caffee-react-a8c42.web.app/"},
  {src: "UI", title: "CSS-Animation", text: "Small web page with interactive animation: CSS transitions and transforms", link: "https://viki-korotkaya.github.io/css-animation/"},
  {src: "Code", title: "Netflex Roulette", text: "Simple App for searching, adding and removing movies, was build using React and Redux. Link to github repository", link: "https://github.com/viki-korotkaya/netflex_roulette"},
];

export const feedbacks = [
  {feedback: 'I was Viktoryia’s mentor in 2016-2017 when she got her first official frontend full-time position as a junior frontend developer. Over that period Viktoryia showed her best as an extremely motivated, hardworking and curious student. She confidently and passionately worked towards her goal to become a front-end developer. In 2019-2020 Viktoryia helped me with mentoring.by the project as a volunteer. She worked on a wide range of tasks as a junior full-stack developer with a huge list of responsibilities: from HTML/CSS markup to a complex JS client and backend code. Viktoryia, I want to say «Thank you!» for all the things you’ve done and your hard work! I was really pleased to work with you, and hope that we will be able to work side by side in the future!', reporter: {name: 'Nikita Schetko, Senior Frontend Developer', citeUrl: 'https://www.linkedin.com/in/nikitaschetko' } }
];

export const aboutMe = 'Let me tell you a little bit about myself. I am a Software Engineer with 2 years of experience on the Front-End side. I got my first job as Junior Front-End Developer more than 5 years ago, and I worked at a software company for almost 1 year. After that I had a big gap in my career. But I tried my best to keep developing my skills. I helped as a Volunteer Developer for a while. I successfully completed EPAM Systems Upskill "Front-End Development" program and accepted an offer from Epam System, where I worked as Software Developer from December 2021 to November 2022. Throughout my career path I have worked with such technologies as JavaScript, React, Redux, TypeScript, Angular, HTML, and CSS. I was participating in different type of development tasks, like creating markups from figma design, fixing styles issues, creating new components, adding UI functionality, writing unit tests. I have experience working in Agile environment. Now I am open to new opportunity.';

export const addressList = [
  {
    name: '609 591 0502',
    icon: 'faPhoneAlt',
    href: 'tel:1-609-591-0502'
  },
  {
    name: 'viki.karotkaya@gmail.com',
    icon: 'faEnvelope',
    href: 'mailto: viki.karotkaya@gmail.com'
  },
  {
    name: 'Twitter',
    icon: 'faTwitter',
    details: 'https://twitter.com/pavlvik1',
    href: 'https://twitter.com/pavlvik1'
  },
  {
    name: 'Facebook',
    icon: 'faFacebookF',
    details: 'https://www.facebook.com/viktoria.korotkaya/',
    href: 'https://www.facebook.com/viktoria.korotkaya/'
  },
  {
    name: 'Skype',
    icon: 'faSkype',
    details: 'viktoria_korotkaya',
    href: "skype:echo123?call"
  }
];
