import * as brandIcons from '@fortawesome/free-brands-svg-icons';
import * as freeIcons from "@fortawesome/free-solid-svg-icons";

export const icons =   {
  ...brandIcons,
  ...freeIcons
}
