import React from "react";
import { render, fireEvent, screen } from './test-utils';
import {Router} from 'react-router-dom';
import { createMemoryHistory } from 'history'
import App from './App';

const history = createMemoryHistory()
const renderWithRouter = (component) => {
  return {
    ...render (
      <Router history={history}>
        {component}
      </Router>
    )
  }
}

test('should render the home page and Inner page after click the button', async() => {
  renderWithRouter(<App />)
  expect(screen.getByText(/Viktoryia Karotkaya/i)).toBeInTheDocument();
  fireEvent.click(screen.getByText('Know more'));
  expect(screen.queryAllByText('About me')).toHaveLength(2);
})
