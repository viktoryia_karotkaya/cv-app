import { Route, Switch } from "react-router-dom";
import Home from "./pages/Home/Home";
import Inner from "./pages/Inner/Inner";
import {personalInfo} from "./utils/data";

function App() {
  return (
    <>
      <Switch>
        <Route path="/" exact>
          <Home
            name={personalInfo.name}
            title={personalInfo.title}
            description={personalInfo.description}
          />
        </Route>
    
        <Route path="/inner" exact>
          <Inner
            name={personalInfo.name}
            title={personalInfo.title}
          />
        </Route>
      </Switch>
      
    </>
  );
}

export default App;
