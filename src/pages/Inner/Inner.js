import {useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronUp} from "@fortawesome/free-solid-svg-icons";
import { animateScroll as scroll } from 'react-scroll';
import Portfolio from "../../components/Portfolio/Portfolio";
import Address from "../../components/Address/Address";
import Panel from "../../components/Panel/Panel";
import Box from "../../components/Box/Box";
import TimeLine from "../../components/TimeLine/TimeLine";
import Expertise from "../../components/Expertise/Expertise";
import Feedback from "../../components/Feedback/Feedback";
import Skills from "../../components/Skills/Skills";
import {expertise, feedbacks, aboutMe} from "../../utils/data";
import Reporter from "../../assets/images/reporter.png";
import "./Inner.scss";

const Inner = (props) => {
  const [isOpen, setIsOpen] = useState(true);
 
  const toggleMenu = () => {
      setIsOpen(!isOpen);
  };
  
  const scrollToTop = () => {
    scroll.scrollToTop();
  };
  
  return(
    <div className="Inner">
      <Panel
        isOpen={isOpen}
        name={props.name}
        handler={toggleMenu}
      />
      <div className={ isOpen ? "Inner-wrapper" : "Inner-wrapper Hidden"} >
        <Box title="About me">
          <article>
            {aboutMe}
          </article>
        </Box>
        <Box title="Education">
          <TimeLine  />
        </Box>
        <Box  title="Experience">
          <Expertise data={expertise} />
        </Box>
        <Box title="Skills">
          <Skills />
        </Box>
        <Box title="Portfolio">
          <Portfolio />
        </Box>
        <Box title="Contacts">
          <Address />
        </Box>
        <Box title="Feedbacks">
          {feedbacks.map((item, index) => <Feedback photo={Reporter} key={index} data={item} />)}
        </Box>
        <div className="ScrollToTop" onClick={scrollToTop}>
          <FontAwesomeIcon icon={faChevronUp} />
        </div>
      </div>
    </div>
  );
}

export default Inner;
