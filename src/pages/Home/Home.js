import {Link} from "react-router-dom";
import PhotoBox from "../../components/PhotoBox/PhotoBox";
import Button from "../../components/Button/Button";
import "./Home.scss";

const Home = (props) => {
  return (
    <div className="Home">
      <div className="Home-wrapper">
        <PhotoBox
          name={props.name}
          title={props.title}
          description={props.description}
        />
        <Link to="/inner" >
          <Button text="Know more"/>
        </Link>
      </div>
    </div>
  )
}

export default Home;
