import "./Expertise.scss";

const Expertise = ({data}) => {
  return (
    <div className="Expertise">
      <ul>
        {data.map((item) =>
          <li key={item.date.toString()}>
            <div className="Expertise-date">
              <h3>{item.info.company}</h3>
              <span>{item.date}</span>
            </div>
            <div className="Expertise-info">
              <h3>{item.info.job}</h3>
              <p>{item.info.description}</p>
            </div>
          </li>
        )}
      </ul>
    </div>
  );
}

export default Expertise;
