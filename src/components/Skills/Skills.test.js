import React from "react";
import { render, screen, fireEvent } from '../../test-utils';
import Skills from "./Skills";

test("initial render with Open edit button", () => {
  render(<Skills />);
  const button = screen.getByText('Open edit');
  expect(button).toBeInTheDocument();
});

test('click the button Open edit and get Close edit button', () => {
  render(<Skills />);
  const button = screen.getByText(/Open edit/);
  fireEvent.click(button);
  expect(screen.getByText(/Close edit/i)).toBeInTheDocument();
});
