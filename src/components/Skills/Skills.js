import {useEffect, useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import {fetchSkills, postSkills} from "../../features/skills/skillsSlice";
import FormEditSkills from "../FormEditSkills/FormEditSkills";
import Button from "../Button/Button";
import SkillChart from "../SkillChart/SkillChart";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import "./Skills.scss";

const Skills = () => {
  const [editIsOpen, setEditIsOpen] = useState(false);
  const dispatch = useDispatch();
  const { skills, status, error } = useSelector((state) => state.skills);
  
  const toggleEditForm = () => {
    setEditIsOpen(!editIsOpen);
  }
  
  useEffect(() => {
    if (status === 'idle'){
      dispatch(fetchSkills());
    }
  }, [status, dispatch]);
  
  useEffect(() => {
    if (error){
      console.log(error)
    }
  }, [error, dispatch]);
  
  const submitHandler = (newSkill) => {
    dispatch(postSkills(newSkill));
    setEditIsOpen(false);
  }
  
  return (
    <div className="Skills-wrapper">
      <Button
        icon={<FontAwesomeIcon icon={faEdit} />}
        text={editIsOpen ? "Close edit" : "Open edit"}
        clickMethod={toggleEditForm}
      />
      {editIsOpen &&
        <FormEditSkills submitHandler={submitHandler} />
      }
      <SkillChart skills={skills} />
    </div>
  )
}

export default Skills;
