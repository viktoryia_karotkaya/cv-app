import "./Skill.scss";

const Skill = (props) => {
  return (
    <div
      className="Skill"
      style={{width: `${props.skill.skillRange}%`}}
    >
      {props.skill.skillName}
    </div>
  )
};

export default Skill;
