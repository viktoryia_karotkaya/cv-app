import React from "react";
import { render, screen } from '../../test-utils';
import Skill from "./Skill";

test("renders Skill with title Test", () => {
  render(<Skill skill={{skillName: "Test", skillRange: 10}} />);
  expect(screen.getByText(/Test/i)).toBeInTheDocument();
});
