import Info from "../Info/Info";
import "./Feedback.scss";

const Feedback = ({photo, data}) => {
  return (
    <div className="Feedback">
      <figure>
        <blockquote cite={data.reporter.citeUrl}>
          <Info text={data.feedback} />
        </blockquote>
        <figcaption><strong>{data.reporter.name}</strong>, <a href={data.reporter.citeUrl} className="Reporter-site">{data.reporter.citeUrl}</a></figcaption>
      </figure>
    </div>
  );
}

export default Feedback;
