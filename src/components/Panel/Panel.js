import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronLeft} from "@fortawesome/free-solid-svg-icons";
import Navigation from "../Navigation/Navigation";
import PhotoBox from "../PhotoBox/PhotoBox";
import Button from "../Button/Button";
import "./Panel.scss";

const Panel = ({isOpen, handler, name}) => {
  return (
    <div className="Panel">
      <aside
        className={isOpen ? "Sidebar" : "Sidebar Hidden"}>
        <PhotoBox
          size="small"
          name={name}
        />
        <Navigation />
        <Link to="/" >
          <Button
            icon={<FontAwesomeIcon icon={faChevronLeft} />}
            text="Go back"
          />
        </Link>
      </aside>
      <div
        className={isOpen ? "Hamburger" : "Hamburger Hidden"}
        onClick={handler}
      >
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
    
  );
}

export default Panel;
