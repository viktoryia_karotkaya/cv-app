import React from "react";
import { render } from '../../test-utils';
import { createMemoryHistory } from 'history'
import {Router} from 'react-router-dom';
import Panel from "./Panel";

const history = createMemoryHistory()
const renderWithRouter = (component) => {
  return {
    ...render (
      <Router history={history}>
        {component}
      </Router>
    )
  }
}

test("renders Panel with opened sidebar", () => {
  const { container } = renderWithRouter(<Panel isOpen={true} />);
  expect(container.firstChild.classList.contains('Hidden')).toBe(false);
});

test("renders Panel with closed sidebar", () => {
  const { container } = renderWithRouter(<Panel isOpen={false} />);
  expect(container.getElementsByClassName('Sidebar Hidden').length).toBe(1);
});
