import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import "./FormEditSkills.scss";

const FormEditSkills = ({submitHandler}) => {
  return (
    <Formik
      initialValues={{ skillName: '', skillRange: '' }}
      validationSchema={Yup.object({
        skillName: Yup.string()
          .trim()
          .required('Skill name is a required field'),
        skillRange: Yup.number().typeError('Skill range has to be a number')
          .min(10, "Skill range must be greater than or equal to 10")
          .max(100, 'Skill range must be less than or equal to 100')
          .required('Skill range is a required field'),
      })}
      onSubmit={ (values, { setSubmitting, resetForm }) => {
        submitHandler(values);
        resetForm();
      }}
    >
      {(props) => (
        <Form className="Form">
          <div className="Input-wrapper">
            <label htmlFor="skillName">Skill name:</label>
            <Field
              className={props.errors.skillName ? "Formik-input Input-notvalid" : "Formik-input"}
              name="skillName"
              type="text"
              placeholder="Enter skill name"
            />
            {props.errors.skillName ? <span className="Error">{props.errors.skillName}</span> : null}
          </div>
          <div className="Input-wrapper">
            <label htmlFor="skillRange">Skill range: </label>
            <Field
              className={props.errors.skillRange ? "Formik-input Input-notvalid" : "Formik-input"}
              name="skillRange" type="text" placeholder="Enter skill range"
            />
            {props.errors.skillRange ? <span className="Error">{props.errors.skillRange}</span> : null}
          </div>
          <button
            disabled={ !props.isValid || !props.dirty || props.isSubmitting}
            type="submit"
          >
            Add skill
          </button>
        </Form>
      )}
    </Formik>
  );
}

export default FormEditSkills;
