import {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSyncAlt} from "@fortawesome/free-solid-svg-icons";
import {fetchEducation} from "../../features/education/educationSlice";
import "./TimeLine.scss";


const TimeLine = () => {
  const dispatch = useDispatch();
  const { education, status, error } = useSelector((state) => state.education);
  
  useEffect(() => {
    if (status === 'idle'){
      dispatch(fetchEducation());
    }
  }, [status, dispatch]);
  
  return (
    <div className="Timeline">
      {status === 'loading' && (
        <div className="Status Icon">
          <FontAwesomeIcon size="3x" className="fa-spin" icon={faSyncAlt} />
        </div>
      )}
      {status === 'failed' && (
        <div className="Status Error">
          {error}
        </div>
      )}
      {
        status === 'succeeded' &&
        <ul>
          {education.map((item) =>
            <li className="Timeline-item" key={item.date.toString() + item.title}>
              <div className="Timeline-date">{item.date}</div>
              <article>
                <h3>{item.title}</h3>
                <p>{item.text}</p>
              </article>
            </li>
          )}
        </ul>
      }
    </div>
  );
};

export default TimeLine;
