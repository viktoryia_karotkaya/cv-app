import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {icons} from "../../utils/icons";
import {navigation} from "../../utils/data";
import {Link} from 'react-scroll'
import "./Navigation.scss";

const Navigation = () => {
  return (
    <nav className="Navigation">
      <ul>
        {navigation.map((el) =>
          <li key={el.id}>
            <Link  activeClass="active" to={el.id} spy={true} smooth={true} duration={500}>
              <div className="Item">
                <div className="Icon"><FontAwesomeIcon icon={icons[el.icon]} /></div>
                <div className="Item-name">{el.item}</div>
              </div>
            </Link>
          </li>)}
      </ul>
    </nav>
  );
}

export default Navigation;
