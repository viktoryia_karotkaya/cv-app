import {useRef, useState, useEffect} from "react";
import Isotope from 'isotope-layout';
import {portfolio} from "../../utils/data";
import Code from "../../assets/images/code.png";
import UI from "../../assets/images/ui.png";
import PortfolioItem from "../PortfolioItem/PortfolioItem";
import "./Portfolio.scss";


const Portfolio = () => {
  const isotope = useRef();
  const [filterKey, setFilterKey] = useState('*');
  
  useEffect(() => {
    isotope.current = new Isotope('.filter-container', {
      itemSelector: '.filter-item',
      layoutMode: 'fitRows'
    })
    return () => isotope.current.destroy()
  }, [])
  
  useEffect(() => {
    filterKey === '*'
      ? isotope.current.arrange({filter: `*`})
      : isotope.current.arrange({filter: `.${filterKey}`})
  }, [filterKey])
  
  const handleFilterKeyChange = key => () => setFilterKey(key)
  
  return (
    <>
      <ul className="Breadcrumb">
        <li className={filterKey === '*' ? "Clicked" : ""} onClick={handleFilterKeyChange('*')}>All</li>
        <li className={filterKey === 'code' ? "Clicked" : ""} onClick={handleFilterKeyChange('code')}>Code</li>
        <li className={filterKey === 'ui' ? "Clicked" : ""} onClick={handleFilterKeyChange('ui')}>UI</li>
      </ul>
      
      <ul className="filter-container Filter">
        {portfolio.map((item) => <PortfolioItem key={item.title} project={item} picture={item.src === "UI" ? UI : Code} />)}
      </ul>
    </>
  )
}

export default Portfolio;
