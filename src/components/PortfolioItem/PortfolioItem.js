import "./PortfolioItem.scss";

const PortfolioItem = ({project, picture}) => {
  return (
    <div className={project.src === "UI" ? "filter-item ui" : "filter-item code"}>
      <img src={picture} alt="project"/>
      <div className="Hovering-wrapper">
        <article>
          <h3>{project.title}</h3>
          <p>{project.text}</p>
          <a href={project.link}>View source</a>
        </article>
      </div>
    </div>
  )
}

export default PortfolioItem;
