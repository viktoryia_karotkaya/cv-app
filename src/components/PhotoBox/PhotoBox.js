import Avatar from "../../assets/images/avatar-big.png";
import "./PhotoBox.scss";

const PhotoBox = ({size, name, title, description}) => {
  return(
    <figure className={size === 'small' ? "PhotoboxSmall" : "Photobox"}>
      <div className="Avatar">
        <img
          src={Avatar}
          alt="avatar"
        />
      </div>
      <figcaption>
        <strong className="Name">{name}</strong>
        <article>
          <header className="Title">{title}</header>
          <div>
            {description}
          </div>
        </article>
      </figcaption>
    </figure>
  );
}

export default PhotoBox;
