import "./Info.scss";

const Info = (props) => {
  return (
    <p className="Info">{props.text}</p>
  );
};

export default Info;
