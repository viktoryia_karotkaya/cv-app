import Skill from "../Skill/Skill";
import "./SkillChart.scss";

const SkillChart = ({skills}) => {
  return (
    <div className="Skillchart">
      <div className="Skillset">
        {skills.map((skill) =>
          <Skill key={skill.id} skill={skill} />
        )}
      </div>
      
      <div className="Ruler">
        <hr/>
        <div className="Label-wrapper">
          <div className="Label">
            <div className="Line"></div>
            <div className="Title">Beginner</div>
          </div>
          <div className="Label">
            <div className="Line"></div>
            <div className="Title">Proficient</div>
          </div>
          <div className="Label">
            <div className="Line"></div>
            <div className="Title">Expert</div>
          </div>
          <div className="Label">
            <div className="Line"></div>
            <div className="Title">Master</div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SkillChart;
