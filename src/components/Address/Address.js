import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {icons} from "../../utils/icons";
import {addressList} from "../../utils/data";
import "./Address.scss";

const Address = () => {
  return (
      <ul className="Address">
        {addressList.map((item) =>
          <li key={item.name}>
            <a href={item.href}>
              <div className="Address-icon">
                <FontAwesomeIcon className="icon" icon={icons[item.icon]} />
              </div>
              <div className="Address-info">
                <p>
                  <b>{item.name}</b><br/>
                  {item.details}
                </p>
              </div>
            </a>
          </li>
        )}
      </ul>
  )
}

export default Address;
