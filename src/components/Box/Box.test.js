import React from "react";
import { render, screen } from '../../test-utils';
import Box from "./Box";

test("renders Box with test titile Test section", () => {
  render(<Box title="Test section" />);
  expect(screen.getByText(/Test section/i)).toBeInTheDocument();
});
