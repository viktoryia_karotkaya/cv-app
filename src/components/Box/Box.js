import "./Box.scss";

const Box = (props) => {
  return (
    <section className="Box" id={props.title.split(" ")[0]}>
      <h2>{props.title}</h2>
      {props.children}
    </section>
  );
};

export default Box;
