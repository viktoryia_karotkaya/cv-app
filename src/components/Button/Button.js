import "./Button.scss";

const Button = (props) => {
  return (
    <div className="Button-wrapper" onClick={props.clickMethod}>
      {props.icon && <span className="Button Button-icon">
          {props.icon}
        </span>}
        <span className="Button Button-name">
          &nbsp;{props.text}
        </span>
    </div>
  );
}

export default Button;
