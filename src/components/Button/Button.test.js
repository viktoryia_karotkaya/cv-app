import React from "react";
import { render, screen } from '../../test-utils';
import Button from "./Button";

test("renders Button with text My button", () => {
  render(<Button text="My button" />);
  expect(screen.getByText(/My button/i)).toBeInTheDocument();
});
