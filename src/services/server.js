import { Server, Model, Response } from "miragejs"

export function makeServer({ environment = "test" } = {}) {
  let server = new Server({
    
    models: {
      education: Model,
      skill: Model
    },
    
    seeds(server) {
      const listOfSkills = getSkillsFromLocalStorage();
      if (listOfSkills.length > 0) {
        listOfSkills.forEach((item) =>
          server.create("skill", item)
        )
      }
      server.create("education", {
        "date": 2021,
        "title": "EPAM Systems",
        "text": "Front-End Development Program"
      })
      server.create("education", {
        "date": 2003,
        "title": "Belarus National Technical University",
        "text": "Engineer in \"Powder Metallurgy and Composite Materials\" field"
      })
      
    },
    routes() {
      this.namespace = "api"
  
      this.get("/education", (schema, request) => {
        return schema.educations.all();
      }, { timing: 3000 })
  
      this.get("/skills", (schema) => {
        return schema.skills.all();
      })
  
      this.post("/skills", (schema, request) => {
        let body = JSON.parse(request.requestBody);
        let skillsInLS = getSkillsFromLocalStorage();
        skillsInLS.push(body);
        postSkillsOnLocalStorage(skillsInLS);
        return schema.skills.create(body);
      })
    },
  })
  
  return server;
}

const getSkillsFromLocalStorage = () => {
  return JSON.parse(localStorage.getItem('skills') || "[]");
}

const postSkillsOnLocalStorage = (list) => {
  localStorage.setItem('skills', JSON.stringify(list));
}
